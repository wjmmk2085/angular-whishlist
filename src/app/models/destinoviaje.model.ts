export class DestinoViaje {

  nombre: string;
  imgUrl: string;

  constructor(nombre: string, url: string) {
      this.nombre = nombre;
      this.imgUrl = url;
  }
}
